# Car Rental Project
A Web Application that acts as a car rental company. 
User can view the available cars, then pay for the access to the selected package, and finally pick up his car. 
Project made for learning purpose.



H2 console:
http://localhost:8080/h2-console

Swagger UI:
http://localhost:8080/swagger-ui/index.html

Open API specification:
http://localhost:8080/v3/api-docs

Testing:
https://www.baeldung.com/spring-boot-testing
https://www.baeldung.com/spring-mvc-test-exceptions
https://junit.org/junit5/docs/snapshot/user-guide/#writing-tests


Validation of schema and API
https://www.baeldung.com/java-validation

Bean validation
https://www.baeldung.com/spring-boot-bean-validation




HOW TO
Compare JSON, JSON Objects
https://www.baeldung.com/jackson-compare-two-json-objects

HOW TO Serialize / Deserialize
JSON <-> Object
````
String jsonString = "{
  "libraryname": "My Library",
  "mymusic": [
    {
      "Artist Name": "Aaron",
      "Song Name": "Beautiful"
    },
    {
      "Artist Name": "Britney",
      "Song Name": "Oops I did It Again"
    },
    {
      "Artist Name": "Britney",
      "Song Name": "Stronger"
    }
  ]
}"

ObjectMapper mapper = new ObjectMapper();
Map<String,Object> map = mapper.readValue(json, Map.class);

JsonNode rootNode = mapper.readTree(json);

public class Library {
  @JsonProperty("libraryname")
  public String name;

  @JsonProperty("mymusic")
  public List<Song> songs;
}
public class Song {
  @JsonProperty("Artist Name") public String artistName;
  @JsonProperty("Song Name") public String songName;
}

Library lib = mapper.readValue(jsonString, Library.class);
````

````
//json input
{
    "id" : "junk",
    "stuff" : "things"
}

//json input
[{
    "id" : "junk",
    "stuff" : "things"
},
{
    "id" : "spam",
    "stuff" : "eggs"
}]

//Java
List<MyClass> entries = ?

import com.fasterxml.jackson.databind.ObjectMapper;
ObjectMapper mapper = new ObjectMapper();

MyClass[] myObjects = mapper.readValue(json, MyClass[].class);

List<MyClass> myObjects = mapper.readValue(jsonInput, new TypeReference<List<MyClass>>(){});

List<MyClass> myObjects = mapper.readValue(jsonInput, mapper.getTypeFactory().constructCollectionType(List.class, MyClass.class));

````