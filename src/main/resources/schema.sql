CREATE TABLE Cars
(
    id                   int NOT NULL AUTO_INCREMENT,
    name                 varchar NOT NULL,
    mark_of_car          varchar NOT NULL,
    model_of_car         varchar NOT NULL,
    segment              varchar,
    img_src_of_car       varchar,
    type_of_fuel         varchar,
    climatronic          boolean,
    electric_shield      boolean,
    electric_mirrors     boolean,
    stationary_computer  boolean,
    automatic_door_close boolean,
    capacity_of_engine   varchar,
    horse_power          varchar,
    fuel_burn_in_city    varchar,
    fuel_burn_on_road    varchar,
    amount_of_doors      int,
    amount_of_air_bags   int,
    abs                  boolean,
    daily_price          decimal,
    PRIMARY KEY (id)
);

CREATE TABLE Users
(
    id       int     NOT NULL AUTO_INCREMENT,
    name     varchar NOT NULL,
    code     varchar NOT NULL COMMENT 'Replaces the username. Auto generated',
    role     int     NOT NULL,
    password varchar,
    token    binary(16),
    PRIMARY KEY (id),
    CONSTRAINT users_unique UNIQUE (code)
);

CREATE TABLE Rents
(
    id        int AUTO_INCREMENT,
    user_id   int       NOT NULL,
    car_id    int       NOT NULL,
    rent_at   TIMESTAMP NOT NULL,
    return_at TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT rents_cars_id_fk FOREIGN KEY (car_id) REFERENCES Cars (id),
    CONSTRAINT rents_user_id_fk FOREIGN KEY (user_id) REFERENCES Users (id)
);