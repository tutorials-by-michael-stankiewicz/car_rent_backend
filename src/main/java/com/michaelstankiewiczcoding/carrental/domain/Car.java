/**
 * Documenting Spring Rest Controller for Car Rental System
 * Copyright (C) 2023 michaelstankiewiczcoding <michaelstankiewiczcoding@gmail.com>
 *
 * Creative Commons Attribution-ShareAlike 4.0 International License
 *
 * Under this license, you are free to:
 * # Share - copy and redistribute the material in any medium or format
 * # Adapt - remix, transform, and build upon the material for any purpose,
 *   even commercially.
 *
 * The licensor cannot revoke these freedoms
 * as long as you follow the license terms.
 *
 * License terms:
 * # Attribution - You must give appropriate credit, provide a link to the
 *   license, and indicate if changes were made. You may do so in any
 *   reasonable manner, but not in any way that suggests the licensor
 *   endorses you or your use.
 * # ShareAlike - If you remix, transform, or build upon the material, you must
 *   distribute your contributions under the same license as the original.
 * # No additional restrictions - You may not apply legal terms or
 *   technological measures that legally restrict others from doing anything the
 *   license permits.
 *
 * Notices:
 * # You do not have to comply with the license for elements of the material in
 *   the public domain or where your use is permitted by an applicable exception
 *   or limitation.
 * # No warranties are given. The license may not give you all of
 *   the permissions necessary for your intended use. For example, other rights
 *   such as publicity, privacy, or moral rights may limit how you use
 *   the material.
 *
 * You may obtain a copy of the License at
 *   https://creativecommons.org/licenses/by-sa/4.0/
 *   https://creativecommons.org/licenses/by-sa/4.0/legalcode
 */
package com.michaelstankiewiczcoding.carrental.domain;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.validation.annotation.Validated;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;


@Validated
@Entity
@Table(name = "cars")
@Data
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Car implements Serializable {

    //TODO
    //correct validation for domain

    @Serial
    private static final long serialVersionUID = 4048798961366546485L;

    @Schema(description = "Unique identifier of the Car.", example = "1")
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "Name of the car.", example = "Toyota Yaris")
    @NotNull
    @Size(min = 1, max = 100, message = "Name of the car to display should be between 3 to 100 characters")
    @Column(name = "NAME")
    private String name;

    @Schema(description = "Mark of the car.", example = "Toyota")
    @NotNull
    @Size(min = 1, max = 30, message = "Mark of the car to should be between 3 to 50 characters")
    @Column(name = "MARK_OF_CAR")
    private String markOfCar;

    @Schema(description = "Model of the car.", example = "Yaris")
    @NotNull
    @Size(min = 1, max = 30, message = "Mark of the car to should be between 3 to 50 characters")
    @Column(name = "MODEL_OF_CAR")
    private String modelOfCar;

    @Schema(description = "Segment of the car.", example = "B")
    @Size(min = 1, max = 30, message = "Segment of the car to should be between 3 to 50 characters")
    @Column(name = "SEGMENT")
    private String segment;

    @Schema(description = "Source of image of car.", example = "https://carfree.pl/galeria/auta/m_201116003731.jpg")
    @NotBlank
    @Column(name = "IMG_SRC_OF_CAR")
    private String imgSrcOfCar;

    @Schema(description = "Type of fuel.", example = "B")
    @Column(name = "TYPE_OF_FUEL")
    private String typeOfFuel;

    @Schema(description = "Is climatronic on board.", example = "TRUE")
    @Column(name = "CLIMATRONIC")
    private boolean climatronic;

    @Schema(description = "Is electric shields on board.", example = "TRUE")
    @Column(name = "ELECTRIC_SHIELD")
    private boolean electricShield;

    @Schema(description = "Is electric mirrors on board.", example = "TRUE")
    @Column(name = "ELECTRIC_MIRRORS")
    private boolean electricMirrors;

    @Schema(description = "Is stationary computer on board.", example = "TRUE")
    @Column(name = "STATIONARY_COMPUTER")
    private boolean stationaryComputer;

    @Schema(description = "Is automatic doors on board.", example = "TRUE")
    @Column(name = "AUTOMATIC_DOOR_CLOSE")
    private boolean automaticDoorClose;

    @Schema(description = "Engine capacity.", example = "1.5")
    @Size(max = 30)
    @Column(name = "CAPACITY_OF_ENGINE")
    private String capacityOfEngine;

    @Schema(description = "Horse power.", example = "111 KM")
    @Size(max = 30)
    @Column(name = "HORSE_POWER")
    private String horsePower;

    @Schema(description = "Fuel burn in city.", example = "5,6 l")
    @Size(max = 30)
    @Column(name = "FUEL_BURN_IN_CITY")
    private String fuelBurnInCity;

    @Schema(description = "Fuel burn on road.", example = "6,5 l")
    @Size(max = 30)
    @Column(name = "FUEL_BURN_ON_ROAD")
    private String fuelBurnOnRoad;

    @Schema(description = "Amount of doors.", example = "5")
    @Max(20)
    @Column(name = "AMOUNT_OF_DOORS")
    private int amountOfDoors;

    @Schema(description = "Amount of air bags.", example = "5")
    @Max(20)
    @Column(name = "AMOUNT_OF_AIR_BAGS")
    private int amountOfAirBags;

    @Schema(description = "Is ABS on board.", example = "TRUE")
    @Column(name = "ABS")
    private boolean abs;

    @Schema(description = "Daily price of car.", example = "200.00")
    @Column(name = "daily_price")
    private BigDecimal dailyPrice;
}