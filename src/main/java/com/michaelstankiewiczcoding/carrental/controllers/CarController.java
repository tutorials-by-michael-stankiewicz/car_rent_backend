/**
 * Documenting Spring Rest Controller for Car Rental System
 * Copyright (C) 2023 michaelstankiewiczcoding <michaelstankiewiczcoding@gmail.com>
 * <p>
 * Creative Commons Attribution-ShareAlike 4.0 International License
 * <p>
 * Under this license, you are free to:
 * # Share - copy and redistribute the material in any medium or format
 * # Adapt - remix, transform, and build upon the material for any purpose,
 * even commercially.
 * <p>
 * The licensor cannot revoke these freedoms
 * as long as you follow the license terms.
 * <p>
 * License terms:
 * # Attribution - You must give appropriate credit, provide a link to the
 * license, and indicate if changes were made. You may do so in any
 * reasonable manner, but not in any way that suggests the licensor
 * endorses you or your use.
 * # ShareAlike - If you remix, transform, or build upon the material, you must
 * distribute your contributions under the same license as the original.
 * # No additional restrictions - You may not apply legal terms or
 * technological measures that legally restrict others from doing anything the
 * license permits.
 * <p>
 * Notices:
 * # You do not have to comply with the license for elements of the material in
 * the public domain or where your use is permitted by an applicable exception
 * or limitation.
 * # No warranties are given. The license may not give you all of
 * the permissions necessary for your intended use. For example, other rights
 * such as publicity, privacy, or moral rights may limit how you use
 * the material.
 * <p>
 * You may obtain a copy of the License at
 * https://creativecommons.org/licenses/by-sa/4.0/
 * https://creativecommons.org/licenses/by-sa/4.0/legalcode
 */
package com.michaelstankiewiczcoding.carrental.controllers;

import com.michaelstankiewiczcoding.carrental.domain.Car;
import com.michaelstankiewiczcoding.carrental.exception.BadResourceException;
import com.michaelstankiewiczcoding.carrental.exception.ResourceAlreadyExistsException;
import com.michaelstankiewiczcoding.carrental.exception.ResourceNotFoundException;
import com.michaelstankiewiczcoding.carrental.repositories.CarRepository;
import com.michaelstankiewiczcoding.carrental.services.CarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api", produces = APPLICATION_JSON_VALUE)
@Tag(name = "cars", description = "the Cars API")
@Validated
public class CarController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    CarRepository carRepository;

    @Autowired
    private CarService carService;

    @Operation(summary = "Find Cars by name", description = "Name search by %name% format", tags = {"car"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = Car.class))))})
    @GetMapping(value = "/cars", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public ResponseEntity<List<Car>> getAllCars(
            @Parameter(description = "Name of the car for search.") @RequestParam(required = false) String name
    ) {
        try {

            List<Car> cars = new ArrayList<Car>(carService.findAll());

            if (cars.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            if (StringUtils.isEmpty(name)) {
                return ResponseEntity.ok(carService.findAll());
            } else {
                return ResponseEntity.ok(carService.findAllByName(name));
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Find car by ID", description = "Returns a single car", tags = {"car"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation",
                    content = @Content(schema = @Schema(implementation = Car.class))),
            @ApiResponse(responseCode = "404", description = "Car not found")})
    @GetMapping(value = "/cars/{id}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public ResponseEntity<Car> getCarById(@PathVariable("id") long id) {
        try {
            Car car = carService.findById(id);
            return ResponseEntity.ok(car);  // return 200, with json body
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null); // return 404, with null body
        }
    }

    @Operation(summary = "Add a new car", description = "", tags = {"car"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Car created",
                    content = @Content(schema = @Schema(implementation = Car.class))),
            @ApiResponse(responseCode = "400", description = "Invalid input"),
            @ApiResponse(responseCode = "409", description = "Car already exists")})
    @PostMapping(value = "/cars", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public ResponseEntity<Car> addCar(
            @Parameter(description = "Car to add. Cannot null or empty.",
                    required = true, schema = @Schema(implementation = Car.class))
            @Valid @RequestBody Car car)
            throws URISyntaxException {
        try {
            Car newCar = carService.save(car);
            return ResponseEntity.created(new URI("/api/cars/" + newCar.getId()))
                    .body(car);
        } catch (ResourceAlreadyExistsException ex) {
            // log exception first, then return Conflict (409)
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } catch (BadResourceException ex) {
            // log exception first, then return Bad Request (400)
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @Operation(summary = "Update an existing car", description = "", tags = {"car"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "400", description = "Bad Request or Validation Error"),
            @ApiResponse(responseCode = "404", description = "Car not found")})
    @PutMapping(value = "/cars/{carId}", consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    public ResponseEntity<Void> updateCar(
            @Parameter(description = "Id of the car to be update. Cannot be empty.",
                    required = true)
            @PathVariable long carId,
            @Parameter(description = "Car to update. Cannot null or empty.",
                    required = true, schema = @Schema(implementation = Car.class))
            @Valid @RequestBody Car car) {
        try {
            car.setId(carId);
            carService.update(car);
            return ResponseEntity.ok().build();
        } catch (ResourceNotFoundException ex) {
            // log exception first, then return Not Found (404)
            logger.error(ex.getMessage());
            return ResponseEntity.notFound().build();
        } catch (BadResourceException ex) {
            // log exception first, then return Bad Request (400)
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @Operation(summary = "Deletes a car", description = "", tags = {"car"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation"),
            @ApiResponse(responseCode = "404", description = "Car not found")})
    @DeleteMapping(path = "/cars/{carId}")
    public ResponseEntity<Void> deleteCarById(
            @Parameter(description = "Id of the car to be delete. Cannot be empty.",
                    required = true)
            @PathVariable long carId) {
        try {
            carService.deleteById(carId);
            return ResponseEntity.ok().build();
        } catch (ResourceNotFoundException ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

}
