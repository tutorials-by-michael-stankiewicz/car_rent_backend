/**
 * Documenting Spring Rest Controller for Car Rental System
 * Copyright (C) 2023 michaelstankiewiczcoding <michaelstankiewiczcoding@gmail.com>
 *
 * Creative Commons Attribution-ShareAlike 4.0 International License
 *
 * Under this license, you are free to:
 * # Share - copy and redistribute the material in any medium or format
 * # Adapt - remix, transform, and build upon the material for any purpose,
 *   even commercially.
 *
 * The licensor cannot revoke these freedoms
 * as long as you follow the license terms.
 *
 * License terms:
 * # Attribution - You must give appropriate credit, provide a link to the
 *   license, and indicate if changes were made. You may do so in any
 *   reasonable manner, but not in any way that suggests the licensor
 *   endorses you or your use.
 * # ShareAlike - If you remix, transform, or build upon the material, you must
 *   distribute your contributions under the same license as the original.
 * # No additional restrictions - You may not apply legal terms or
 *   technological measures that legally restrict others from doing anything the
 *   license permits.
 *
 * Notices:
 * # You do not have to comply with the license for elements of the material in
 *   the public domain or where your use is permitted by an applicable exception
 *   or limitation.
 * # No warranties are given. The license may not give you all of
 *   the permissions necessary for your intended use. For example, other rights
 *   such as publicity, privacy, or moral rights may limit how you use
 *   the material.
 *
 * You may obtain a copy of the License at
 *   https://creativecommons.org/licenses/by-sa/4.0/
 *   https://creativecommons.org/licenses/by-sa/4.0/legalcode
 */
package com.michaelstankiewiczcoding.carrental.services;

import com.michaelstankiewiczcoding.carrental.domain.Car;
import com.michaelstankiewiczcoding.carrental.exception.BadResourceException;
import com.michaelstankiewiczcoding.carrental.exception.ResourceAlreadyExistsException;
import com.michaelstankiewiczcoding.carrental.exception.ResourceNotFoundException;
import com.michaelstankiewiczcoding.carrental.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    private boolean existsById(Long id) {
        return carRepository.existsById(id);
    }

    public Car findById(Long id) throws ResourceNotFoundException {
        Car car = carRepository.findById(id).orElse(null);
        if (car==null) {
            throw new ResourceNotFoundException("Cannot find Car with id: " + id);
        }
        else return car;
    }

    public List<Car> findAll() {
        return new ArrayList<>(carRepository.findAll());
    }

    public List<Car> findAllByName(String name) {
        return new ArrayList<>(carRepository.findByName(name));
    }

    public Car save(Car car) throws BadResourceException, ResourceAlreadyExistsException {
        if (!StringUtils.isEmpty(car.getName())) {
            if (car.getId() != null && existsById(car.getId())) {
                throw new ResourceAlreadyExistsException("Car with id: " + car.getId() +
                        " already exists");
            }
            return carRepository.save(car);
        }
        else {
            BadResourceException exc = new BadResourceException("Failed to save Car");
            exc.addErrorMessage("Car is null or empty");
            throw exc;
        }
    }

    public void update(Car car)
            throws BadResourceException, ResourceNotFoundException {
        if (!StringUtils.isEmpty(car.getName())) {
            if (!existsById(car.getId())) {
                throw new ResourceNotFoundException("Cannot find Contact with id: " + car.getId());
            }
            carRepository.save(car);
        }
        else {
            BadResourceException exc = new BadResourceException("Failed to save car");
            exc.addErrorMessage("Car is null or empty");
            throw exc;
        }
    }

    public void deleteById(Long id) throws ResourceNotFoundException {
        if (!existsById(id)) {
            throw new ResourceNotFoundException("Cannot find car with id: " + id);
        }
        else {
            carRepository.deleteById(id);
        }
    }

    public Long count() {
        return carRepository.count();
    }

}
