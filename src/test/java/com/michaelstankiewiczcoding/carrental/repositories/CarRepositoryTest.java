package com.michaelstankiewiczcoding.carrental.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michaelstankiewiczcoding.carrental.domain.Car;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureJson
class CarRepositoryTest {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ResourceLoader resourceLoader;

    @BeforeEach
    void setup() throws Exception {
        String initialCarsAsString = resourceLoader.getResource("classpath:data/initial_cars.json").getContentAsString(StandardCharsets.UTF_8);
        List<Car> initialCars = objectMapper.readValue(initialCarsAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, Car.class));
        carRepository.saveAll(initialCars);
    }

    @AfterEach
    void tearDown() {
        carRepository.deleteAll();
    }

    @Test
    void shouldReturnCarsWithClimatronic() {

        List<Car> cars = carRepository.findCars(PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "id")));

        assertThat(carRepository.findClimatronicCars(PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "id"))))
                .isEqualTo(cars.stream()
                        .filter(Car::isClimatronic)
                        .collect(Collectors.toList()));
    }

    @Test
    void shouldReturnCarsWithAbs() {

        List<Car> cars = carRepository.findCars(PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "id")));

        assertThat(carRepository.findAbsCars(PageRequest.of(1, 10, Sort.by(Sort.Direction.ASC, "id"))))
                .isEqualTo(cars.stream()
                        .filter(Car::isAbs)
                        .collect(Collectors.toList()));
    }


}