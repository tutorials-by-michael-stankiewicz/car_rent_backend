package com.michaelstankiewiczcoding.carrental.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michaelstankiewiczcoding.carrental.domain.Car;
import com.michaelstankiewiczcoding.carrental.exception.ResourceNotFoundException;
import com.michaelstankiewiczcoding.carrental.repositories.CarRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ResourceLoader;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

/**
 * (webEnvironment = SpringBootTest.WebEnvironment.NONE) - because we don't want to load web layer such as @Controllers
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
/**
 * needed for @BeforeAll use, in other case Test class wouldn't know how to create before data
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CarServiceTest {

    @Autowired
    private CarService carService;

    @MockBean
    private CarRepository carRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ResourceLoader resourceLoader;

    Car car1;
    Car car2;
    Car car3;
    List<Car> cars;

    @BeforeAll
    void createTestCars() throws Exception {
        //create 3 sample cars, and add them to our repository
        car1 = objectMapper.readValue(resourceLoader.getResource("classpath:data/1_car.json").getContentAsString(StandardCharsets.UTF_8), Car.class);
        car2 = objectMapper.readValue(resourceLoader.getResource("classpath:data/2_car.json").getContentAsString(StandardCharsets.UTF_8), Car.class);
        car3 = objectMapper.readValue(resourceLoader.getResource("classpath:data/3_car.json").getContentAsString(StandardCharsets.UTF_8), Car.class);
        cars = new ArrayList<>();
        cars.addAll(List.of(car1, car2, car3));
    }

    @Test
    void shouldFindAllCars() {
        when(carRepository.findAll()).thenReturn(cars);

        Assertions.assertEquals(carService.findAll(), cars);
    }

    @Test
    void shouldFindCarById() throws ResourceNotFoundException {
        when(carRepository.findById(1L)).thenReturn(Optional.ofNullable(car1));

        Assertions.assertEquals(carService.findById(1L), car1);
    }



}