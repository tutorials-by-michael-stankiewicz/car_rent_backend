/**
 * Documenting Spring Rest Controller for Car Rental System
 * Copyright (C) 2023 michaelstankiewiczcoding <michaelstankiewiczcoding@gmail.com>
 *
 * Creative Commons Attribution-ShareAlike 4.0 International License
 *
 * Under this license, you are free to:
 * # Share - copy and redistribute the material in any medium or format
 * # Adapt - remix, transform, and build upon the material for any purpose,
 *   even commercially.
 *
 * The licensor cannot revoke these freedoms
 * as long as you follow the license terms.
 *
 * License terms:
 * # Attribution - You must give appropriate credit, provide a link to the
 *   license, and indicate if changes were made. You may do so in any
 *   reasonable manner, but not in any way that suggests the licensor
 *   endorses you or your use.
 * # ShareAlike - If you remix, transform, or build upon the material, you must
 *   distribute your contributions under the same license as the original.
 * # No additional restrictions - You may not apply legal terms or
 *   technological measures that legally restrict others from doing anything the
 *   license permits.
 *
 * Notices:
 * # You do not have to comply with the license for elements of the material in
 *   the public domain or where your use is permitted by an applicable exception
 *   or limitation.
 * # No warranties are given. The license may not give you all of
 *   the permissions necessary for your intended use. For example, other rights
 *   such as publicity, privacy, or moral rights may limit how you use
 *   the material.
 *
 * You may obtain a copy of the License at
 *   https://creativecommons.org/licenses/by-sa/4.0/
 *   https://creativecommons.org/licenses/by-sa/4.0/legalcode
 */
package com.michaelstankiewiczcoding.carrental;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michaelstankiewiczcoding.carrental.domain.Car;
import com.michaelstankiewiczcoding.carrental.repositories.CarRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CarApiIntegrationTest {

    //Ideally, we should keep the integration tests separated from the unit tests and should not run along with the unit tests.
    //Running tests in order should be correct, repository should have cars from previous test
    //Running single test could fail, cause repository can be in wrong state

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ResourceLoader resourceLoader;

    @Test
    @Order(1)
    @DisplayName("Get cars with empty repository")
    public void givenEmptyRepositoryReturnNoContent() throws Exception {
        carRepository.deleteAll();
        carRepository.flush();
        mvc.perform(get("/api/cars")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @Order(2)
    @DisplayName("Get cars with initial data of 10 cars")
    public void givenInitialDataReturnALlCars() throws Exception {
        String initialCarsAsString = resourceLoader.getResource("classpath:data/initial_cars.json").getContentAsString(StandardCharsets.UTF_8);
        List<Car> initialCars = objectMapper.readValue(initialCarsAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, Car.class));
        carRepository.saveAll(initialCars);

        mvc.perform(get("/api/cars")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(initialCarsAsString));
    }

    @Order(3)
    @ValueSource(ints = {1, 5, 7, 10})
    @ParameterizedTest(name = "Get car with ID {0}.")
    @DisplayName("Get car with given ID.")
    public void givenCarWithIdReturnThisCar(int carId) throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/" + carId + "_car.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(get("/api/cars/" + carId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(content().json(carToTestInStringFormat));
    }

    @Test
    @Order(4)
    @DisplayName("Get car with not existing id")
    public void givenNonExistingCarIdShouldReturnNotFound() throws Exception {
        mvc.perform(get("/api/cars/11")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(5)
    @DisplayName("Add car that don't exist")
    public void givenNotExistingCarAddThisCar() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_add.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(post("/api/cars")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isCreated());
    }

    @Test
    @Order(6)
    @DisplayName("Add car with invalid input")
    public void givenWrongJustShouldReturnInvalidInput() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_add_invalid_input.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(post("/api/cars")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(7)
    @DisplayName("Add car that already exist")
    public void givenCarThatAlreadyExistShouldReturnConflict() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_add_that_already_exist.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(post("/api/cars")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isConflict());
    }

    @Test
    @Order(8)
    @DisplayName("Update car that exist")
    public void givenCarThatAlreadyExistUpdateIt() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_update.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(put("/api/cars/11")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isOk());
    }

    @Test
    @Order(9)
    @DisplayName("Update car with wrong id")
    public void givenCarIdThatDontExistShouldReturn() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_update.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(put("/api/cars/12")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(10)
    @DisplayName("Update car, but wrong request")
    public void givenWrongRequestToUpdateShouldReturnBadRequest() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_update_bad_request.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(put("/api/cars/11")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(11)
    @DisplayName("Update car, but not valid with entity")
    public void givenNotValidRequestToUpdateShouldReturnBadRequest() throws Exception {
        String carToTestInStringFormat = resourceLoader.getResource("classpath:data/car_to_update_validation_exception.json").getContentAsString(StandardCharsets.UTF_8);

        mvc.perform(put("/api/cars/11")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8)
                        .content(carToTestInStringFormat))
                .andExpect(status().isBadRequest());
    }


    @Test
    @Order(12)
    @DisplayName("Delete car that exist")
    public void givenCarThatAlreadyExistDeleteIt() throws Exception {
        mvc.perform(delete("/api/cars/11")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8))
                .andExpect(status().isOk());
    }

    @Test
    @Order(13)
    @DisplayName("Delete car with wrong id")
    public void givenCarIdThatDontExistShouldReturnNotFound() throws Exception {
        mvc.perform(delete("/api/cars/12")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding(StandardCharsets.UTF_8))
                .andExpect(status().isNotFound());
    }

}